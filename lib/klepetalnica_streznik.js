var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};


//nalaganje datoteke v predpomnilnik
var predpomnilnikV = {};
var fs  = require('fs');
function naloziStaticnoVsebinoVBesed(predpomnilnikV, absolutnaPotDoDatoteke) {
  if (predpomnilnikV[absolutnaPotDoDatoteke]) {
    //posredujDatoteko(odgovor, absolutnaPotDoDatoteke, predpomnilnikV[absolutnaPotDoDatoteke]);
    return;
  } else {
    fs.exists(absolutnaPotDoDatoteke, function(datotekaObstaja) {
      if (datotekaObstaja) {
        fs.readFile(absolutnaPotDoDatoteke, function(napaka, datotekaVsebina) {
          if (napaka) {
            //posredujNapako404(odgovor);
          } else {
            predpomnilnikV[absolutnaPotDoDatoteke] = datotekaVsebina;
            //posredujDatoteko(odgovor, absolutnaPotDoDatoteke, datotekaVsebina);
          }
        });
      } else {
        //posredujNapako404(odgovor);
      }
    });
  }
}

//
var vzdevkiGledeNaSocket2 = {};

//
var trenutniKanalGeslo = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    
    //
    obdelajPosredovanjeZasebnegaSporocila(socket);
    naloziStaticnoVsebinoVBesed(predpomnilnikV, "./public/swearWords.txt");
    //
    
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    
    //
    socket.on('disconnect', function () {
      var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
      delete uporabljeniVzdevki[vzdevekIndeks];
      delete vzdevkiGledeNaSocket[socket.id];
    
      //posodobimo seznam uporabnikov
      obdelajSeznamUporabnikov(socket, trenutniKanal[socket.id]);
      //
    });
    
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal, geslo) {
  //ce kanal nima gesla
  var vstop = false;
  if(trenutniKanalGeslo[kanal] === undefined){
    //socket.emit('sporocilo', {besedilo: "nima gesla "});
    if(geslo === undefined){
      vstop = true;
      //socket.emit('sporocilo', {besedilo: "ni kljuca "});
    }else{
        //
        
        //if(trenutniKanal[socket.id] !== undefined){
        if(kanal in trenutniKanal){
          //kanal ze obstaja in imamo kljuc
          socket.emit('sporocilo', {besedilo: "Izbrani kanal " +  kanal + " je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom."});
          vstop = false;
        }else{
          //kanal ne obstaja in imamo kljuc
          //socket.emit('sporocilo', {besedilo: "ima kjuc, torej naredi novo sobo s kljucem "});
          trenutniKanalGeslo[kanal] = geslo;
          vstop = true;
        }
      
    }
  
  }else if(trenutniKanalGeslo[kanal] == geslo){
    //ce ima geslo in je pravilno
    //socket.emit('sporocilo', {besedilo: "ima geslo + pravilen kljuc "});
    vstop = true;
  }else if(trenutniKanalGeslo[kanal] != geslo){
    vstop = false;
    //neuspesno 
    //socket.emit('sporocilo', {besedilo: "napacno geslo " + trenutniKanalGeslo[kanal] + " "+ geslo});
    socket.emit('sporocilo', {besedilo: "Pridružitev v kanal " +  kanal + " ni bilo uspešno, ker je geslo napačno!"});
  }
  
  if(vstop === true){
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  
  //spremenimo emit za prikaz uporabnika
  socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek: vzdevkiGledeNaSocket[socket.id]});
  
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

//
vzdevkiGledeNaSocket2[vzdevkiGledeNaSocket[socket.id]] = socket;

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }

  
  //posodobimo seznam uporabnikov
  obdelajSeznamUporabnikov(socket, kanal);
  //
  
  }
}

//nova funkcija za obdelanje seznama uporabnikov
function obdelajSeznamUporabnikov(socket, kanal) {
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var upNaKanalu;

  if (uporabnikiNaKanalu.length > 0) {
    upNaKanalu = vzdevkiGledeNaSocket[uporabnikiNaKanalu[0].id];
    for (var i in uporabnikiNaKanalu) {
      
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != uporabnikiNaKanalu[0].id) {
        //pripravimo uporabnike na izpis
        
        if(typeof vzdevkiGledeNaSocket[uporabnikSocketId] !== 'undefined'){
          upNaKanalu += '\n' + vzdevkiGledeNaSocket[uporabnikSocketId];
        }
      }
      
    }
  }
  
  //posljemo seznam uporabnikov na nov socket
  io.sockets.emit("seznamUporabnikovNaKanalu", {uporabniki: upNaKanalu});

  }
  


//

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        
        //
        vzdevkiGledeNaSocket2[vzdevkiGledeNaSocket[socket.id]] = socket;
        
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
        
        //posodobimo seznam uporabnikov
        obdelajSeznamUporabnikov(socket, trenutniKanal[socket.id]);
        //
        
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function filtrirajVulgarneBesede(besedilo){
  //nalozimo datoteko vulgarnih besed, ce se ni
  naloziStaticnoVsebinoVBesed(predpomnilnikV, "./public/swearWords.txt");
  //maskiranje vulgarnih besed
    //vneseno besedilo razdelimo po presledkih, ter shranimo kot tabelo na spremnljivo "str"
    var str = besedilo.split(" "); 
    var besede = predpomnilnikV["./public/swearWords.txt"].toString().split("\n");
    for(var o = 0; o < besede.length; o++){
      besede[o] = besede[o].substring(0, besede[o].length - 1);
    }
    for(var i = 0; i < str.length; i++){

              if (besede.indexOf(str[i]) > -1) {
                //ujeta vulgarna beseda 
                var dolzinaMaskiraneBesede = str[i].length;
                str[i] = "*";
                for(var j = 1; j < dolzinaMaskiraneBesede; j++){
                   str[i] = str[i] + "*";
                }
                break; 
              }
      
    }
    //nastavi zamiaskirano besedilo sporocila 
    besedilo =  str.join(" ");
    
    return besedilo;
}

function obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket) {
  socket.on('sporocilo', function (sporocilo) {
    
    //filtriramo besedilo
    sporocilo.besedilo = filtrirajVulgarneBesede(sporocilo.besedilo);

    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {       
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}


//funkcija za zasebna sporocila
function obdelajPosredovanjeZasebnegaSporocila(socket){
  socket.on("zasebnoSporocilo", function(naslovnik, besedilo){
    
    //preveri ce obstaja
    //poslji, sicer vrni napako posiljatelju
    //filtriramo besedilo
    besedilo = filtrirajVulgarneBesede(besedilo);
    
    if(naslovnik in vzdevkiGledeNaSocket2 && vzdevkiGledeNaSocket[socket.id] != naslovnik){
      vzdevkiGledeNaSocket2[naslovnik].emit('sporocilo', { besedilo: vzdevkiGledeNaSocket[socket.id] + " (zasebno): " + besedilo });
    }else{
      socket.emit('sporocilo', { besedilo: "Sporočila " + besedilo + " uporabniku z vzdevkom " + naslovnik + " ni bilo mogoče posredovati." });
    }
  });
}
//

//spremenimo funkcijo da omogocimo zascitene kanale
function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}