var Klepet = function(socket) {
  this.socket = socket;
};

//
function divElementKombiniranTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').append(sporocilo);
}

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

//nova funkcija za zasciten kanal
Klepet.prototype.spremeniZascitenKanal = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};
//

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      //var kanal = besede.join(' ');
      
      //preverimo ce je ukaz z gesmol
      var kanal = besede[0];
      //this.socket.emit('sporocilo', {besedilo: kanal});
      if(besede.length == 2){
        kanal = kanal.substring(1, kanal.length -1);
        besede.shift();
        var geslo = besede[0].substring(1, besede[0].length -1);
        //this.socket.emit('sporocilo', {besedilo: kanal + " "+geslo});
        this.spremeniZascitenKanal(kanal, geslo);
      }else{
        this.spremeniKanal(kanal);
      }
     
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
      
    //ce posljemo zasebo sporocilo
    case "zasebno":
      besede.shift();
      var naslovnik = besede[0].substring(1, besede[0].length -1);
      besede.shift();
      besede = besede.join(' ');
      besede = besede.substring(1, besede.length -1);
      var besedilo = besede;
      this.socket.emit("zasebnoSporocilo", naslovnik, besedilo);
      $('#sporocila').append(divElementKombiniranTekst("(Zasebno za " + naslovnik + "): " + besedilo));
      break;
    //
      
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};