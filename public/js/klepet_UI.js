function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementKombiniranTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').append(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  
  if(sporocilo.indexOf(";)") > -1){
    sporocilo = sporocilo.replace(/\;\)/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\">");
  }
  if(sporocilo.indexOf(":)") > -1){
    sporocilo = sporocilo.replace(/\:\)/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\">");
  }
  if(sporocilo.indexOf("(y)") > -1){
    sporocilo = sporocilo.replace(/\(y\)/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\">");
  }
  if(sporocilo.indexOf(":*") > -1){
    sporocilo = sporocilo.replace(/\:\*/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\">");
  }
  if(sporocilo.indexOf(":(") > -1){
    sporocilo = sporocilo.replace(/\:\(/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\">");
  }

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

    //klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text().slice( $('#kanal').text().indexOf("@") + 2 ), sporocilo);
    
    $('#sporocila').append(divElementKombiniranTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.'
      
      //ob spremebi vzdevka posodbimo labelo kanala
      $('#kanal').text(rezultat.vzdevek + " @ " + $('#kanal').text().slice( $('#kanal').text().indexOf("@") + 1 ));
      
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  //nov socket za seznam uporabnikov na kanalu
  socket.on('seznamUporabnikovNaKanalu', function(seznamUporabnikovNaKanalu){
    $('#seznam-uporabnikov').empty();
    $('#seznam-uporabnikov').append( divElementEnostavniTekst(seznamUporabnikovNaKanalu.uporabniki) );
  });
  //

  socket.on('pridruzitevOdgovor', function(rezultat) {
    
    //ob spremebi vzdevka posodbimo labelo kanala
    $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
    
    //$('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  //preuredimo funkcijo za sporocila
  socket.on('sporocilo', function (sporocilo) {
    //var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    //$('#sporocila').append(novElement);
    $('#sporocila').append(divElementKombiniranTekst(sporocilo.besedilo));
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});